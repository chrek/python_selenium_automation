import pytest

from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

@pytest.fixture()
def browser():
    opts = Options()
    #opts.headless = True
    opts.headless = False
    driver = Firefox(executable_path="drivers/geckodriver", options=opts)

    driver.implicitly_wait(10)

    yield driver

    # Quit driver to allow for cleanup
    driver.quit()


def test_greet_form(browser):
    user_name = "Chrek"
    user_city = "New York"

    #browser.get('http://localhost:5000/')
    browser.get('http://localhost:80/')

    form = browser.find_element_by_id("frm")
    name = browser.find_element_by_name("name")
    name.send_keys(user_name)

    city = browser.find_element_by_name("city")
    city.send_keys(user_city)

    form.submit()

    WebDriverWait(browser, 3).until(ec.url_matches('/greet'), 'Timed out waiting for resonse')

    content = browser.page_source
    print(content)

    # assert 'Hello Chrek' in content
    assert 'New York' in content


