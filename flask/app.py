from flask import Flask, render_template, request

app = Flask(__name__)

@app.route("/")
def home():
    return app.send_static_file('greet.html')


@app.route("/greet")
def greet():
    username = request.args.get('name')
    city = request.args.get('city')
    return render_template('index.html', name=username, city=city)


if __name__ == "__main__":
    app.run()
    # app.run(host='0.0.0.0', port=80)
