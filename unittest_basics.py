import unittest

from selenium import webdriver

from selenium.webdriver.firefox.options import Options

class WebCodePage(unittest.TestCase):
    def setUp(self):
        opts = Options()
        opts.headless = True

        self.driver = webdriver.Firefox(executable_path="drivers/geckodriver", options=opts)

    def test_title(self):
        self.driver.get("http://webcode.me")
        self.assertIn("My html page", self.driver.title)

    def test_paragraphs(self):
        self.driver.get('http://webcode.me')

        elms = self.driver.find_elements_by_tag_name("p")

        for elm in elms:
            print(elm.text)

        self.assertIn("Today is a beautiful day", elms[0].text)
        self.assertIn("Hello there", elms[1].text)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()