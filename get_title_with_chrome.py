from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options

opts = Options()
opts.headless = True

driver = Chrome(executable_path="drivers/chromedriver.exe", options=opts)

try:
    driver.get('http://webcode.me')
    print(driver.title)
    assert 'My html page' == driver.title

finally:
    driver.quit()