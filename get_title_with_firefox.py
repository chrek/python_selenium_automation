from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options

opts = Options()
opts.headless = True

driver = Firefox(executable_path="drivers/geckodriver", options=opts)

try:
    driver.get('http://webcode.me')
    print(driver.title)
    assert 'My html page' == driver.title

finally:
    driver.quit()