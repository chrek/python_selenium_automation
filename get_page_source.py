from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options

opts = Options()
opts.headless = True

driver = Firefox(executable_path="drivers/geckodriver", options=opts)

try:
    driver.get('http://webcode.me')
    title = driver.title
    content = driver.page_source

    print(content)

    # assert title == 'My html page'
    assert 'My html page' in title
    assert 'Today is a beautiful day' in content

finally:
    driver.quit()
