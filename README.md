# python_selenium_automation

Automate web application tests with Selenium framework in Python.

## Application Under Test:
1. http://webcode.me/
2. A Basic Flask Application (see folder named flask) running on localhost

## Browser Drivers used 
* geckodriver 0.30.0 (2021-09-16, d372710b98a6) for Firefox Release 98.0.1
* ChromeDriver 99.0.4844.51

## Python unit testing framework
Using the unittest is a Python unit testing framework.
The unittest supports test automation, the sharing of setup and
shutdown code for tests.

### The test suite (unittest_basics.py) checks for the title and paragraphs of the webcode.me home page
- Get and Assert the page title
- The page_source property gets the source of the current page.
- Get and print the text of the two paragraphs on the page
- Test for a JavaScript alert box

## Python Selenium Testing with pytest
The pytest framework makes it easy to write small, readable tests, and can 
scale to support complex functional testing for applications and libraries.

### The test suite (tests/test_with_pytest.py) checks for the title and paragraphs of the webcode.me home page
- Get and Assert the page title

### Flask Application Under Test
A Test case (flask/tests/flask_web_test.py) with pytest and Selenium for a Flask web application testing.

The Flask application is a simple form in which you are expected to submit some information and the application will display a welcome page.

### Start the Flask Application with the commands below from the application directory:
- set FLASK_APP=app.py
- set FLASK_ENV=development
- flask run --host=0.0.0.0 --port=80

### Run the Test Cases from another command line
With the Flask application running, execute the pytest using the command below from the project directory:

- pytest

----

## References

1.	[zetcode](https://zetcode.com/python/selenium/)
2.  [pytest.org](https://docs.pytest.org/en/7.1.x/)