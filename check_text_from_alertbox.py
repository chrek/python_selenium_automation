from time import sleep
from pathlib import Path

from selenium.common.exceptions import TimeoutException
from selenium.webdriver import Firefox
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.firefox.options import Options

pagefile = Path.cwd()/"index.html"

# opts = Options()
# opts.headless = True

# driver = Firefox(executable_path="drivers/geckodriver", options=opts)
driver = Firefox(executable_path="drivers/geckodriver")

try:
    driver.get(f'file://{pagefile}')
    button = driver.find_element_by_id("btn")
    sleep(20)
    button.click()
    sleep(13)

    try:
        WebDriverWait(driver, 3).until(ec.alert_is_present(), 'Timed out waiting for confirmation for popup to appear')
        alert = driver.switch_to.alert
        assert alert.text == 'Hello from alert box!'

        alert.accept()
        print("Alert accepted")

    except TimeoutException:
        print("No Alert")

    sleep(5)

finally:
    driver.quit()