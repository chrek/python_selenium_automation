from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options

opts = Options()
opts.headless = True

driver = Firefox(executable_path="drivers/geckodriver", options=opts)

try:
    driver.get('http://webcode.me')

    elms = driver.find_elements_by_tag_name("p")

    for elm in elms:
        print(elm.text)


finally:
    driver.quit()
